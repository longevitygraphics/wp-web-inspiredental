<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>

	<?php if (!is_page("community-partners") && !is_page("community-involvement")): ?>
		<?php if (have_rows("community_partners", "option")): ?>
			<?php $title = get_field("community_partners_title", "option") ?>
			<div class="community-partners px-4 py-4 bg-lightgrey">
				<?php if ($title): ?>
					<div class="community-partner-title">
						<?php echo $title; ?>
					</div>
				<?php endif ?>
				<div class="community-partners-wrapper">
				<?php while(have_rows("community_partners", "option")) : the_row(); ?>
				<?php  
					$image = get_sub_field("image", "option");
					$link = get_sub_field("link", "option");
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<div>
					<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" class="partner-icon">
					<img class="px-2" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					</a>
				</div>
				<?php endwhile; ?>
				</div>
			</div>
		<?php endif; ?>
	<?php endif ?>
	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>
		
	<footer id="site-footer">
		<div>
			<div>
				<div id="site-footer-main" class="py-4 px-md-3 row justify-content-between align-items-start">
					<?php  
						$office_hours = get_field("office_hours", "options");
						$footer_map = get_field("footer_map", "options");
						$footer_address = get_field("footer_address", "options");
					?>
					<div class="quick-links col-md-6 col-lg-2">
						<h2 class="h5">Quick Links</h2>
						<?php 
					        $footerMenu = array(
					          	// 'menu'              => 'menu-1',
					          	'theme_location'    => 'bottom-nav',
					          	'depth'             => 2,
					        );
					        wp_nav_menu($footerMenu);
				        ?>
				        <?php echo do_shortcode("[lg-social-media]"); ?>
					</div>
					<div class="footer-locations col-md-6 col-lg-2">
						<h2 class="h5">Locations</h2>
						<?php 
					        $footerLocation = array(
					          	'menu'              => 'locations-menu',
					          	'theme_location'    => 'max_mega_menu_1',
					          	'depth'             => 2,
					        );
					        wp_nav_menu($footerLocation);
				        ?>
					</div>
					<div class="footer-hours col-md-6 col-lg-4">
						<?php if ($office_hours): ?>
							<!-- <h2 class="h5">Office Hours</h2> -->
							<?php echo $office_hours ?>
						<?php endif ?>
					</div>
					<div class="footer-instagram-feed col-md-6 col-lg-4">
						<div class="footer-instagram-ajax">
							<?php
								// do_action('example_ajax_request'); 
								// example_ajax_request();
							//echo do_shortcode("[instagram-feed]"); 
							?>
						</div>
					</div>
				</div>
			</div>
			<?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
				<div id="site-legal" class="py-3 px-3">
					<div class="d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
						<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
						<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action('document_end'); ?>
