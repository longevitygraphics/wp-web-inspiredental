// Windows Ready Handler
 	
(function($) {
	
    $(document).ready(function(){
    	//menu parent clickable
        if ($(window).width() > 991) {
            $('.navbar #main-navbar .dropdown').hover(function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
            }, function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
            });

            $('.navbar #main-navbar .dropdown > a').click(function(){
                location.href = this.href;
            });
        } else {
            var timesClicked = 0;
            
            $('.navbar #main-navbar .nav-link').click(function(){
                timesClicked++;
                console.log(timesClicked);
                if(timesClicked==1){
                    $('.navbar #main-navbar .dropdown').hover(function() {
                        $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
                    }, function() {
                        $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
                    });

                    $('.navbar #main-navbar .dropdown > a').click(function(){
                        location.href = this.href;
                    });
                        }
                    })

        }
        
        //end menu parent clickable

    	$('.service-tablist-wrapper').click(function(){
    		$('.nav-tabs').toggleClass('show-nav-tabs');
    	});

        //show overlay on click (for mobile)
        $(".card-overlay-component-wrapper > div").click(function(){
            $(this).children(".card-overlay-content").toggleClass('show-overlay-content');
            console.log("clicked")
        })

        // community partner slider
        $('.community-partners-wrapper').slick({
            lazyLoad: 'ondemand',
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 7000,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        adaptiveHeight: true
                    }
                }
                ]
        });

         // $('.community-partners-wrapper').on('afterChange', function(event, slick, currentSlide, nextSlide) {
         //        $('.community-partners-wrapper').slick('refresh');
         // });
        // thumbnail slickslide
        $('.thumbnail-slickslide').slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: false,
            slide: 'li',
            cssEase: 'linear',
            slidesToShow: 1,
            arrows: true,
            appendDots: $(".slick-thumbnail-dots"),
            autoplay: true,
            autoplaySpeed: 4000,
            adaptiveHeight: true,
            customPaging: function (slider, i) {
                return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
            }
        });
        $('.slick-thumbnail-dots').children('ul').removeClass('slick-dots');

        //two column procedure
        $('.procedures').slick({
            dots: false,
            infinite: true,
            fade: false,
            slidesToShow: 1,
            arrow: true,
            appendArrows: '.slider-nav',
            prevArrow: $('.prev-slide'),
            nextArrow: $('.next-slide'),
            autoplay: true,
            autoplaySpeed: 15000,
            adaptiveHeight: true
        });

        /****************** AJAX Example ********************/
            $(window).on('scroll', function() {
                var element = document.querySelector('footer');
                var position = element.getBoundingClientRect();

                // checking whether fully visible
                if(position.top >= 0 && position.bottom <= window.innerHeight) {
                    console.log('Element is fully visible in screen');
                }

                // checking for partial visibility
                if(position.top < window.innerHeight && position.bottom >= 0) {
                    // We'll pass this variable to the PHP function example_ajax_request
                    var fruit = 'Banana';
                     
                    // This does the ajax request
                    $.ajax({
                        url: example_ajax_obj.ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
                        data: {
                            'action': 'example_ajax_request',
                            'fruit' : fruit
                        },
                        success:function(data) {
                            $('.footer-instagram-ajax').html(data);
                            // sbi_init();
                        },
                        error: function(errorThrown){
                            console.log(errorThrown);
                        }
                    });
                    $(window).off('scroll');
                }
            });

            

            /****************** AJAX Example end ******************/  
    });

}(jQuery));