<?php

	// $image
	$src = $gallery['src'];
	$settings = $gallery['settings'];
	$fade = $settings['fade'];
	$infinite = $settings['infinite'];
	$autoplay = $settings['autoplay'];
	$arrows = $settings['arrows'];
	$dots = $settings['dots'];
	$autoplay_speed = $settings['autoplay_speed'];
	$caption = $settings['caption'];

	global $gallery_id;
	$gallery_id++;

?>

	<?php if($src && is_array($src)): ?>
		<div class="lg-gallery" id="<?php echo $gallery_id; ?>">
			<?php foreach ($src as $key => $value): ?>
				<div>
					<?php if($value['link']): ?><a href="<?php echo $value['link']; ?>"><?php endif; ?>
					<img class="img-full" src="<?php echo $value['image']['url']; ?>?original" alt="<?php echo $value['image']['alt']; ?>">
					<?php if($value['link']): ?></a><?php endif; ?>
					<?php if($caption == 1): ?>
						<div class="caption">
							<?php echo $value['image']['caption']; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>

	<?php function enqueue_to_footer() {

		$src = $gallery['src'];
		$settings = $gallery['settings'];
		$fade = $settings['fade'];
		$infinite = $settings['infinite'];
		$autoplay = $settings['autoplay'];
		$arrows = $settings['arrows'];
		$dots = $settings['dots'];
		$autoplay_speed = $settings['autoplay_speed'];
		$caption = $settings['caption'];

		global $gallery_id;

	?>
		<script type="text/javascript">
			(function($) {

			    $(document).ready(function(){
			    	if($('.lg-gallery#<?php echo $gallery_id; ?>').closest('.variable-height')[0]){
			    		var adaptiveHeight = false;
			    	}else{
			    		var adaptiveHeight = true;
			    	}

			    	$('.lg-gallery#<?php echo $gallery_id; ?>').slick({
						arrows: <?php echo ($arrows == 1 ? 'true' : 'false'); ?>,
						dots: <?php echo ($dots == 1 ? 'true' : 'false'); ?>,
						fade: <?php echo ($fade == 1 ? 'true' : 'false'); ?>,
						autoplay: <?php echo ($autoplay == 1 ? 'true' : 'false'); ?>,
						autoplaySpeed: <?php echo ($autoplay_speed ? $autoplay_speed * 1000 : 5000 ); ?>,
			    		adaptiveHeight: adaptiveHeight
					});
			    });

			}(jQuery));
		</script>
	<?php 
		} // end of enqueue to footer
		add_action( 'wp_footer', 'enqueue_to_footer', 100 ); 
	?>
	