<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!---------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php if (have_rows("testimonials")): ?>
				<div class="row align-items-center">
					<?php while(have_rows("testimonials")): the_row(); ?>
						<?php

						$iframe = get_sub_field('video');
						preg_match('/src="(.+?)"/', $iframe, $matches);
						$src = $matches[1];
						$params = array(
						    'controls'    => 0,
						    'hd'        => 1,
						    'autohide'    => 1
						);
						$new_src = add_query_arg($params, $src);
						$iframe = str_replace($src, $new_src, $iframe);
						$attributes = 'frameborder="0"';
						$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
						?>
						<div class="col-md-6 col-lg-3">
							<div class="p-3">
								<?php echo $iframe; ?>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif ?>
		</div>
	</div>

<!---------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
