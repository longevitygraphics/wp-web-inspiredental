<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!---------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php  
				$col_sm = get_sub_field("column_width_small");
				$col_md = get_sub_field("column_width_medium");
				$col_lg = get_sub_field("column_width_large");
				$col = get_sub_field("column_width");
			?>
			<?php if (have_rows("gallery_images")) : ?>
				<div class="gallery-grid row">
					<?php while(have_rows("gallery_images")) : the_row(); ?>
						<?php 
							$lightbox_img = get_sub_field("gallery_image_lightbox");
							$gallery_content = get_sub_field("gallery_content");
						?>
						<div class="grid-item col-<?php echo $col; ?> col-sm-<?php echo $col_sm; ?> col-md-<?php echo $col_md; ?> col-lg-<?php echo $col_lg; ?> mb-3">
							<div>
							    <a class="gallery-content gallery-img" data-lightbox="roadtrip" href="<?php echo $lightbox_img['url']; ?>">
							        <img class="w-100" src="<?php echo $lightbox_img['url']; ?>" alt="<?php echo $lightbox_img['alt']; ?>">
							    </a>
						    </div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif ?>
		</div>
	</div>

<!---------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
