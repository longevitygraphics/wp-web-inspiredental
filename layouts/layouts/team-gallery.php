<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>



<!---------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php if (have_rows("team_member")): ?>
				<div class="team-members d-flex flex-wrap align-items-stretch">
				<?php while(have_rows("team_member")): the_row(); ?>
					<?php  
						$img_fun = get_sub_field("image_fun");
						$img_serious = get_sub_field("image_serious");
						$title = get_sub_field("title");
						$link = get_sub_field("link");
						if( $link && is_array($link) ) {
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						}
					?>
					<div class="member col-sm-6 col-md-4 d-flex flex-column justify-content-start">
						<div class="member-image">
							<a class="button" <?php if($link): ?>href="<?php echo esc_url($link_url); ?>" <?php endif; ?> target="<?php echo esc_attr($link_target); ?>">
							<img class="image-fun" src="<?php echo $img_fun['url']?>" alt="<?php echo $img_fun['alt']?>">
							<img class="image-serious" src="<?php echo $img_serious['url']?>" alt="<?php echo $img_serious['alt']?>">
							</a>
						</div>
						<div class="member-title">
							<?php echo $title; ?>
						</div>
					</div>
				<?php endwhile ?>
				</div>
			<?php endif ?>
		</div>
	</div>

<!---------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
