<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!---------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php if (have_rows("community_partners", "option")): ?>
				<?php $title = get_field("community_partners_title", "option") ?>
				<div class="community-partners-layout">
					<div class="community-partners-layout-wrapper d-flex flex-wrap justify-content-center justify-content-sm-start align-items-center">
					<?php while(have_rows("community_partners", "option")) : the_row(); ?>
					<?php  
						$image = get_sub_field("image", "option");
						$link = get_sub_field("link", "option");
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
						<div class="mb-3 col-sm-6 col-md-4 col-lg-3">
							<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>" class="partner-icon">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
							</a>
						</div>
					<?php endwhile; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>

<!---------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
