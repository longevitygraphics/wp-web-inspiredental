<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php if (have_rows("slick_thumbnail_content")) : ?>
				<div class="slick-thumbnail-content">
					<div class="slick-thumbnail-dots">
				
					</div>
					<ul class="slickslide thumbnail-slickslide">
					<?php while(have_rows("slick_thumbnail_content")) : the_row(); ?>
						<?php  
							$thumb = get_sub_field("thumb");
							$content = get_sub_field("content");
							$image = get_sub_field("image");
						?>
						<li>
							<div class="content-left"><?php echo $content; ?></div>
							<div class="content-right"><img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>"></div>
						</li>
					<?php endwhile; ?>
					</ul>
					<div class="slick-thumbs">
						<ul>
						<?php while(have_rows("slick_thumbnail_content")) : the_row(); ?>
							<?php  
								$thumb = get_sub_field("thumb");
								$thumb_title = get_sub_field("thumb_title");
								$content = get_sub_field("content");
								$image = get_sub_field("image");
							?>
							<li>
								<img src="<?php echo $thumb['url'];?>" alt="<?php echo $thumb['alt'];?>">
								<p><?php echo $thumb_title; ?></p>
							</li>
						<?php endwhile; ?>
						</ul>
					</div> <!-- end of slick thumbs -->
				</div> <!-- end of slick-thumbnail-content -->
			<?php endif ?>
		</div>
	</div>
<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
