<?php
function main_nav_items ( $items, $args ) {
	//lg_write_log($args);
    if ($args->menu->slug == 'main-menu') {
        $items .= '<li class="d-none d-lg-inline-block nav-phone menu-item menu-item-type-custom menu-item-object-custom nav-item ml-3"><a class="nav-link text-white btn btn-primary" href="tel:' . do_shortcode("[lg-phone-alt]") . '">'. do_shortcode("[lg-phone-main]") .'</a></li>';
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

add_action('wp_content_top', 'featured_banner_top', 1); // ('wp_content_top', defined function name, order)

    function featured_banner_top(){
        ob_start();
        get_template_part('/templates/template-parts/page/top-banner');
        echo ob_get_clean();
    }

/*********************** AJAX example ***************************/
function example_ajax_request() {
 
    if ( isset($_REQUEST) ) {
        $fruit = $_REQUEST['fruit'];

        if ( $fruit == 'Banana' ) {
            $fruit = do_shortcode('[instagram-feed]');
        }
        echo $fruit;
    }
   die();
}
 
add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );

add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );

function example_ajax_enqueue() {
    wp_localize_script(
        'lg-script-child',
        'example_ajax_obj',
        array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) )
    );
}
add_action( 'wp_enqueue_scripts', 'example_ajax_enqueue' );

/*********************** AJAX example end ***************************/

/*************** Move dashicons to the footer *******************/
add_action( 'wp_print_styles', 'deregister_dashicons_styles' );
function deregister_dashicons_styles() {
    wp_deregister_style( 'dashicons' );
}

add_action( 'wp_footer', 'register_dashicon_wp_footer' );
function register_dashicon_wp_footer() {
    wp_enqueue_style( 'dashicons', '/wp-includes/css/dashicons.min.css');
}
?>